# frozen_string_literal: true

# Cell
class Cell
  attr_reader :column, :row
  attr_accessor :north, :south, :east, :west

  def initialize(row, column)
    @column = column
    @row = row
    @links = {}
  end

  def location
    [@row, @column]
  end

  def link(cell, bidirectional = true)
    @links[cell] = true
    cell.link(self, false) if bidirectional
    self
  end

  def unlink(cell, bidirectional = true)
    @links.delete(cell)
    cell.unlink(self, false) if bidirectional
    self
  end

  def links
    @links.keys
  end

  def linked?(cell)
    @links.key?(cell)
  end

  def neighbors
    list = []
    list << north if north
    list << south if south
    list << east if east
    list << west if west
    list
  end

  def inspect
    "Cell: row: #{row} column: #{column} links: #{links}"
  end
end
