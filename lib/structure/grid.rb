
require './lib/structure/cell'

# Grid
class Grid
  attr_reader :columns, :rows

  def initialize(columns, rows)
    @columns = columns
    @rows = rows
    @grid = populate(rows, columns)
    configure
  end

  def size
    @columns * @rows
  end

  def cell(row, column)
    self[row, column]
  end

  def cells
    @grid
  end

  def [](row, column)
    return nil unless row.between?(0, @rows - 1)
    return nil unless column.between?(0, @columns - 1)

    @grid[row][column]
  end

  def random_cell
    self[rand(@rows), rand(@columns)]
  end

  def each_row
    @grid.each do |row|
      yield row
    end
  end

  def each_cell
    each_row do |row|
      row.each do |cell|
        yield cell if cell
      end
    end
  end

  def to_s
    to_s_v2
  end

  def to_s_v2
    output = "+" + "---+" * columns + "\n"

    each_row do |row|
      top = "|"
      bottom = "+"

      row.each do |cell|
        cell = Cell.new(-1, -1) unless cell

        body = " #{contents_of(cell)} "
        east_boundary = (cell.linked?(cell.east) ? " " : "|")
        top << body << east_boundary

        south_boundary = (cell.linked?(cell.south) ? "   " : "---")
        corner = "+"
        bottom << south_boundary << corner
      end

      output << top << "\n"
      output << bottom << "\n"
    end

    output
  end

  def contents_of(cell)
    " "
  end

  private

  def populate(rows, columns)
    Array.new(rows) do |row|
      Array.new(columns) do |column|
        Cell.new(row, column)
      end
    end
  end

  def configure
    each_cell do |cell|
      row         = cell.row
      column      = cell.column
      cell.north  = self[row - 1, column]
      cell.south  = self[row + 1, column]
      cell.east   = self[row, column + 1]
      cell.west   = self[row, column - 1]
    end
  end
end
