#!/usr/bin/env ruby

require './lib/structure/grid'
require './lib/algorithms/binary_tree'
require 'highline'

cli = HighLine.new

grid_columns = cli.ask("Grid columns?  ", Integer) { |q| q.in = 0..50 }
grid_rows = cli.ask("Grid rows?  ", Integer) { |q| q.in = 0..20 }

algorithm = cli.choose do |menu|
  menu.prompt = "Choose maze generation algorithm:  "
  menu.choice(:binary_tree) { cli.say("Using binary tree") }
  menu.default = :binary_tree
end

grid = Grid.new(grid_columns, grid_rows)

case algorithm
when :binary_tree
  BinaryTree.on(grid)
else
  BinaryTree.on(grid)
end

puts grid
