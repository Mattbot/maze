require './lib/structure/cell'
require './lib/structure/grid'

RSpec.describe Cell do
  before do
    @grid     = Grid.new(5, 5)
    @cell_1_2 = Cell.new(1, 2)
    @cell_1_3 = Cell.new(1, 3)
    @cell_1_4 = Cell.new(1, 4)
    @cell_2_2 = Cell.new(2, 2)
  end

  describe '#location' do
    it 'returns correct location for cell' do
      expect(@cell_1_2.location).to eq([1, 2])
    end
  end

  describe '#links' do
    it 'returns links for cell' do
      @cell_1_2.link(@cell_1_3)
      @cell_1_2.link(@cell_1_4)
      expect(@cell_1_2.links).to eq([@cell_1_3, @cell_1_4])
    end
  end

  describe '#unlink' do
    it 'returns links for cell' do
      @cell_1_2.link(@cell_1_3)
      @cell_1_2.link(@cell_1_4)
      @cell_1_2.unlink(@cell_1_4)
      expect(@cell_1_2.links).to eq([@cell_1_3])
    end
  end

  describe '#linked?' do
    it 'returns true if cells linked' do
      @cell_1_2.link(@cell_1_3)
      expect(@cell_1_2.linked?(@cell_1_3)).to eq(true)
    end
    it 'returns false if cells not linked' do
      expect(@cell_1_2.linked?(@cell_1_4)).to eq(false)
    end
  end

  describe '#neighbors' do
    it 'returns neighbors for middle cell' do
      neighbors = '[Cell: row: 0 column: 1 links: [], Cell: row: 2 column: 1 links: [], Cell: row: 1 column: 2 links: [], Cell: row: 1 column: 0 links: []]'
      expect(@grid.cell(1, 1).neighbors.to_s).to eq(neighbors)
    end
    it 'returns neighbors for corner cell' do
      neighbors = '[Cell: row: 1 column: 0 links: [], Cell: row: 0 column: 1 links: []]'
      expect(@grid.cell(0, 0).neighbors.to_s).to eq(neighbors)
    end
  end
end
