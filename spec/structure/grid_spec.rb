
require './lib/structure/grid'
require './lib/structure/cell'

RSpec.describe Grid do
  before do
    @grid = Grid.new(5, 7)
    @cell_1_2 = Cell.new(1, 2)
  end

  describe '#size' do
    it 'returns correct size for grid' do
      expect(@grid.size).to eq(35)
    end
  end

  describe '#cell' do
    it 'returns correct location for cell' do
      expect(@grid.cell(1, 2).location).to eq(@cell_1_2.location)
    end
  end

  describe '#random_cell' do
    it 'returns a random cell' do
      expect(@grid.random_cell.class).to eq(@cell_1_2.class)
    end
  end
end
